use facturacion;
INSERT INTO `facturacion`.`categoria` (`nombre`) VALUES ('Bebidas');
INSERT INTO `facturacion`.`categoria` (`nombre`) VALUES ('Guisantes');
INSERT INTO `facturacion`.`categoria` (`nombre`) VALUES ('Carnes y Embutidos');
INSERT INTO `facturacion`.`categoria` (`nombre`) VALUES ('Frutas y Verduras');
INSERT INTO `facturacion`.`categoria` (`nombre`) VALUES ('Aceite Pasta y Legumbres');
INSERT INTO `facturacion`.`categoria` (`nombre`) VALUES ('Aperitivos');
INSERT INTO `facturacion`.`categoria` (`nombre`) VALUES ('Cosmetica');
INSERT INTO `facturacion`.`categoria` (`nombre`) VALUES ('Limpieza');
INSERT INTO `facturacion`.`categoria` (`nombre`) VALUES ('Juguetes');
INSERT INTO `facturacion`.`categoria` (`nombre`) VALUES ('Comida Preparadas');

INSERT INTO `facturacion`.`producto` (`nombre`, `precio`, `stock`, `id_categoria`) VALUES ('Agua de botella', '0.75', '50', '1');
INSERT INTO `facturacion`.`producto` (`nombre`, `precio`, `stock`, `id_categoria`) VALUES ('Jugo del Valle', '0.65', '25', '1');
INSERT INTO `facturacion`.`producto` (`nombre`, `precio`, `stock`, `id_categoria`) VALUES ('Rinso Surf 500 gr', '1.25', '10', '8');
INSERT INTO `facturacion`.`producto` (`nombre`, `precio`, `stock`, `id_categoria`) VALUES ('Lejia Magia Blanca 100 gr', '2.50', '30', '8');
INSERT INTO `facturacion`.`producto` (`nombre`, `precio`, `stock`, `id_categoria`) VALUES ('Carne roja LB', '4.00', '50', '3');
INSERT INTO `facturacion`.`producto` (`nombre`, `precio`, `stock`, `id_categoria`) VALUES ('Salchichas 10 u', '1.50', '40', '3');
INSERT INTO `facturacion`.`producto` (`nombre`, `precio`, `stock`, `id_categoria`) VALUES ('Lb de Uva', '2.00', '50', '4');
INSERT INTO `facturacion`.`producto` (`nombre`, `precio`, `stock`, `id_categoria`) VALUES ('Melon', '1.0', '100', '4');
INSERT INTO `facturacion`.`producto` (`nombre`, `precio`, `stock`, `id_categoria`) VALUES ('Quesito', '0.90', '30', '6');
INSERT INTO `facturacion`.`producto` (`nombre`, `precio`, `stock`, `id_categoria`) VALUES ('Jalapeño', '0.75', '30', '6');

INSERT INTO `facturacion`.`cliente` (`nombre`, `apellido`, `direccion`, `fecha_nacimiento`, `telefono`, `email`) VALUES ('Juan', 'Sanchez', 'Col. San Joaquin', '1998-03-01', '2225-7500', 'ja@gmail.com');
INSERT INTO `facturacion`.`cliente` (`nombre`, `apellido`, `direccion`, `fecha_nacimiento`, `telefono`, `email`) VALUES ('Carolina', 'Hernandez', 'Col. Santa Maria', '200-03-01', '2225-8000', 'gh@gmail.com');
INSERT INTO `facturacion`.`cliente` (`nombre`, `apellido`, `direccion`, `fecha_nacimiento`, `telefono`, `email`) VALUES ('Isabel', 'Hernandez', 'Canton tecoluco', '1997-03-04', '2550-8545', 'ih@gmail.com');
INSERT INTO `facturacion`.`cliente` (`nombre`, `apellido`, `direccion`, `fecha_nacimiento`, `telefono`, `email`) VALUES ('Maria', 'Franco', 'Col. San Teresa', '1995-04-25', '2005-7789', 'mf@gmail.com');
INSERT INTO `facturacion`.`cliente` (`nombre`, `apellido`, `direccion`, `fecha_nacimiento`, `telefono`, `email`) VALUES ('Mauro', 'Mendoza', 'San jacinto', '1992-10-01', '2578-8752', 'mm@gmail.com');

INSERT INTO `facturacion`.`factura` (`codigo`, `id_cliente`, `fecha`, `num_pago`) VALUES ('cl-001', '1', '2019-11-29', '1');
INSERT INTO `facturacion`.`factura` (`codigo`, `id_cliente`, `fecha`, `num_pago`) VALUES ( 'cl-002', '2', '2019-11-29', '2');
INSERT INTO `facturacion`.`factura` (`codigo`, `id_cliente`, `fecha`, `num_pago`) VALUES ('cl-003', '2', '2019-11-29', '3');

INSERT INTO `facturacion`.`detalle` (`num_factura`, `id_producto`, `cantidad`, `precio`) VALUES ('1', '2', '5', '3.25');
INSERT INTO `facturacion`.`detalle` (`num_factura`, `id_producto`, `cantidad`, `precio`) VALUES ('1', '1', '2', '1.50');