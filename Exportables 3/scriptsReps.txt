/*
Script para exportacion 1.
*/

//El objecto JasperPrint es el reporte precargado
JasperPrint jasperPrint = JasperFillManager.fillReport(reporteJasper, parametros, conexion_o_datasource);

//Objecto exporter de las librerias de jaspereport
JRPdfExporter exporter = new JRPdfExporter();
exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
exporter.setExporterOutput(
new SimpleOutputStreamExporterOutput("employeeReport.pdf"));

//Configuraciones para el diseño del documento
SimplePdfReportConfiguration reportConfig = new SimplePdfReportConfiguration();
reportConfig.setSizePageToContent(true);
reportConfig.setForceLineBreakPolicy(false);

exporter.setConfiguration(reportConfig);

//Metodo de exportacion para el documento. 
exporter.exportReport();


/*
Script para exportacion 2.
*/

public void generatePdfReport(HttpServletResponse response) {
try {

//Si lo que deseamos es no enviar parametros al metodo podemos inicializarlos dentro del mismo metodo.
//Como por ejemplo el objeto response

//Esta linea sirve para cargar el documento de diseño del reporte por medio de un stream de datos.
InputStream jrxml = this.getClass().getClassLoader().getResource("informe.jrxml").openStream();
JasperView vista = JRXmlLoader.load(jrxml);
JasperReport jasperReport = JasperCompileManager.compileReport(vista);

//El objeto jasperprint funciona para guardar la estructura o plantilla final del documento.
JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, new HashMap(), jsonDataSource);

//Los objetos aca sirven para finalmente exportar el documento en formato pdf
JRPdfExporter pdfExporter = new JRPdfExporter();
pdfExporter.setExporterInput(new SimpleExporterInput(jasperPrint));
ByteArrayOutputStream pdfReportStream = new ByteArrayOutputStream();
pdfExporter.setExporterOutput(new SimpleOutputStreamExporterOutput(pdfReportStream));

//Previo a exportar, se debe establecer el tamaño del documento para ser mostrado en la vista
//luego se aplica este metodo para llamar a exportacion.
pdfExporter.exportReport();

//Para finalizar utilizamos el response para definir el formato del reporte
//y ademas descargarlo con el nombre que le asignemos
response.setContentType("application/pdf");
response.setHeader("Content-Length", String.valueOf(pdfReportStream.size()));
response.addHeader("Content-Disposition", "inline; filename=Reportelo.pdf;");


//Debido que cuando realizamos una exportacion debemos cerrar el stream de datos (o la transferencia).
OutputStream responseOutputStream = response.getOutputStream();
responseOutputStream.write(pdfReportStream.toByteArray());
responseOutputStream.close();
pdfReportStream.close();

} catch (Exception e) {
e.printStackTrace();
}

}


/*
Script para exportacion 3.
*/
//Creamos una clase conexion como parte del datasource que necesitaremos para nuestro reporte.
Connection conexion;
Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
conexion = (Connection) DriverManager.getConnection("jdbc:mysql://localhost:3306/facturacion", "root", "admin");


//Obtenemos el reporte realizado en extension .jasper
File reportFile = new File(application.getRealPath("rep/otrocliente.jasper"));
Map parameters = new HashMap();
parameters.put("Nombre_parametro", "Valor_Parametro");

//Agregamos una variable de parametros para luego utilizar el metodo runReportToPdf
//Recordemos que este metodo es estatico por esa razon se puede utilizar solamente el metodo.
//Lo que nos devuelve el un arreglo de bytes, luego de precargar el reporte 
byte[] bytes = JasperRunManager.runReportToPdf(reportFile.getPath(), parameters, conexion);

//Aplicamos la definicion de el reporte especificando la capacidad en bytes para este. 
response.setContentType("application/pdf");
response.setContentLength(bytes.length);

//Creamos un objeto ServletOutPutStream con el cual "escribiremos" nuestro reporte.
ServletOutputStream os = response.getOutputStream();


os.write(bytes, 0, bytes.length);

os.flush();
os.close();

/*
Script para exportacion 4.
*/
//Si utilizaremos JSF para la realizacion de reportes tendriamos que realizar la siguiente estructura.
//Agregar los parametros
Map<String,Object> parametros= new HashMap<String,Object>();
parametros.put("param", "Parameters");

//Obtener el documento .jasper
File jasper = new File(FacesContext.getCurrentInstance().getExternalContext().getRealPath("/rptJSF.jasper"));

//Debido que jasperreport es una libreria de terceros. Podemos enviar un datasource
//utilizando colecciones de datos como sigue a continuacion.
JasperPrint jasperPrint = JasperFillManager.fillReport(jasper.getPath(),parametros, new JRBeanCollectionDataSource(this.getLista()));

//Utilizamos el response desde el contexto del servlet de "Faces"
HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
response.addHeader("Content-disposition","attachment; filename=jsfReporte.pdf");

//Cargamos un 
ServletOutputStream stream = response.getOutputStream();

//Exportamos el documento utilizando el metodo "exportReportToPdfStream
JasperExportManager.exportReportToPdfStream(jasperPrint, stream);

stream.flush();
stream.close();
FacesContext.getCurrentInstance().responseComplete();



/*
Script para exportacion 4.1
*/
//A diferencia del script 4 lo que hace importante esta forma es la DI que se realiza.
//Para utilizar este formato que como se aprecia es para Spring, se debe:
//Realizar la DI del servlet activo en el contexto del proyecto para encontrar el archivo .jasper 
@Autowired
ServletContext servletContext;

public void setServletContext(ServletContext servletContext) {
	this.servletContext = servletContext;
}

@RequestMapping("/estadosAtm")
public void estadoCajeros(HttpServletResponse response, HttpServletRequest request,
		ServletOutputStream outputStream) throws ServletException, IOException {

	//Como observamos tomamos del request el contexto y utilizamos la ubicacion de recursos.
	String ubicacionImagen = request.getSession().getServletContext().getRealPath("/assets/jasperreport/net.png");
	if (null != request.getParameter("paramState") && null != request.getParameter("muniParam")) {
		int datoValor = Integer.parseInt(request.getParameter("paramState"));
		int muniValor = Integer.parseInt(request.getParameter("muniParam"));
		
		//realizamos el paso de parametros con los cuales obtendremos la lista de 
		listaAtm = reportesIn.listaEstado(datoValor, muniValor);

		//Se necesita la el stream para el reporte, es decir, su ubicacion relativa.
		InputStream ubicacionReporte = request.getSession().getServletContext()
				.getResourceAsStream("/assets/jasperreport/EstadosAtm.jasper");
		
		Municipio m = reportesIn.selectByIdMunicipios(muniValor);
		try {
			Map<String, Object> parameter = new HashMap<String, Object>();
			parameter.put("Imagen", ubicacionImagen);
			parameter.put("Municipio", m.getNombre());
			parameter.put("", new String());

			//El metodo estatico es para precargar el reporte final.
			byte[] bytes = JasperRunManager.runReportToPdf(ubicacionReporte, parameter,
					new JRBeanCollectionDataSource(listaAtm));

			//La configuracion sobre como descargaremos el reporte
			response.setContentType("application/pdf");
			response.setContentLength(bytes.length);
			response.setHeader("Content-disposition", "inline; filename=municipios_report.pdf");

			//El OutputStream final.
			outputStream = response.getOutputStream();
			outputStream.write(bytes, 0, bytes.length);

			//Limpiamos y cerramos el buffer para nuevos stream's
			outputStream.flush();
			outputStream.close();

			} catch (Exception e) {

				System.out.println("Error en pdfAtm  " + e.getMessage());
			}

		} else {
			return;
			}
	}